//generate RND number send_query4sum.go

package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"
)

var url string

func main() {

	if len(os.Args) > 1 {
		fmt.Println("ARG:", os.Args[1])
		//link := "http://" + os.Args[1]
		if len(os.Args) > 2 {
			fmt.Println("ARG:", os.Args[2])
			//url := ""
			count, err := strconv.Atoi(os.Args[2])
			// if not number -> error
			if err != nil {
				log.Println("Bad number!")
				return
			} //end_if_error
			for ii := 1; ii <= count; ii++ {

				//fmt.Println(rnd1, rnd2, url)
				go f(count)

				//amt := time.Duration(rand.Intn(10000))
				time.Sleep(time.Millisecond * 10000)

			} //end_if_count

		}
	} else {
		fmt.Println("Example: send_query4sum ip-address count_request")
		fmt.Println("send_query4sum 47.254.44.161 100")
		fmt.Println("send_query4sum websum.awx.com 100")
		fmt.Println("Press Enter to exit")

	} //end_if_exist_arguments

	//fmt.Println("Press Enter to exit")
	//var input string
	//fmt.Scanln(&input)
	//

} //end func main

func f(count int) {
	for i := 1; i <= count; i++ {
		s1 := rand.NewSource(time.Now().UnixNano())
		r1 := rand.New(s1)
		rnd1 := r1.Intn(count)

		s2 := rand.NewSource(time.Now().UnixNano())
		r2 := rand.New(s2)
		rnd2 := r2.Intn(count)
		url = "http://" + os.Args[1] + ":7000/?a=" + strconv.Itoa(int(rnd1)) + "&b=" + strconv.Itoa(int(rnd2))

		fmt.Println(url)
		http.Get(url)
		//delay
		amt := time.Duration(rand.Intn(20000))
		time.Sleep(time.Millisecond * amt)
	}
} //end func go_f
