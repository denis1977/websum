package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"strconv"

	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	db          *sql.DB
	errorsCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "gocalc_errors_count",
			Help: "Gocalc Errors Count Per Type",
		},
		[]string{"type"},
	)
	query_count = 0
	err         error
)

func handler(w http.ResponseWriter, r *http.Request) {

	//keyall := r.URL.String()
	if r.URL.String() != "/favicon.ico" {

		keys, ok := r.URL.Query()["a"]
		if !ok || len(keys[0]) < 1 {
			log.Println("Url Param 'a' is missing")
			return
		}
		a, err := strconv.Atoi(keys[0])
		if err != nil {
			log.Println("Bad number!")
			return
		}

		keys, ok = r.URL.Query()["b"]
		if !ok || len(keys[0]) < 1 {
			log.Println("Url Param 'b' is missing")
			return
		}
		b, err := strconv.Atoi(keys[0])
		if err != nil {
			log.Println("Bad number!")
			return
		}
		c := sum(a, b)
		query_count++
		//output on web
		fmt.Fprintf(w, "%d + %d = %d", a, b, c)
		//output on console
		log.Printf("    %d      %d + %d = %d", query_count, a, b, c)

		// Connecting to database
		//db, err = sql.Open("postgres", cfg.PostgresUri)
		db, err := sql.Open("postgres", "host=10.9.106.9 user=postgres password=telefon dbname=summator sslmode=disable")
		if err != nil {
			log.Fatalf("Can't connect to postgresql: %v", err)
		}
		defer db.Close()
		err = db.Ping()
		if err != nil {
			log.Fatalf("Can't ping database: %v", err)
		}
		query := "INSERT INTO joup (alfa,beta,sigma,addr) values (" + strconv.Itoa(int(a)) + "," + strconv.Itoa(int(b)) + "," + strconv.Itoa(int(c)) + ",'10.9.106.9')"
		result, err := db.Exec(query)
		if err != nil {
			panic(err)
		}
		rowa, erra := result.RowsAffected()
		if erra != nil {
			panic(err)
		} else {
			rowa++
		}
		//fmt.Println("Inserted data")

	} //end_if_keyall
	//if keys, ok = r.URL.Query()["faricon"]

}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":7000", nil))
}

func sum(a, b int) int {
	t := a + b
	return t
}
