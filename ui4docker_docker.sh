#!/bin/bash

docker run -d --rm --name=UI4Docker -p 9009:9000 --privileged -v /var/run/docker.sock:/var/run/docker.sock uifd/ui-for-docker
sleep 1
docker ps -a | grep 'UI4Docker'
