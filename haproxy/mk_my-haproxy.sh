#!/bin/bash

docker build -t my-haproxy .
sleep 1
docker images | grep 'proxy'
echo "run validate haproxy config"
docker run -it --rm --name haproxy-syntax-check my-haproxy haproxy -c -f /usr/local/etc/haproxy/haproxy.cfg
echo "run haproxy backend"
docker run -d --rm --name my-haproxy7 -p 9000:9000 -p 7000:7000 my-haproxy
sleep 3
docker ps -a | grep 'haproxy'
echo "Check one more time"
sleep 3
docker ps -a | grep 'haproxy'
echo "This is good!"