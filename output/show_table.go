package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

type product struct {
	count int
	sigma int
}

var (
	db          *sql.DB
	query_count = 0
	err         error
	CCount      int
)

func main() {
	//host := "10.9.106.9"
	db, err := sql.Open("postgres", "host=10.9.106.9 port=54321 user=postgres password=telefon dbname=summator sslmode=disable")
	if err != nil {
		log.Fatalf("Can't connect to postgresql: %v", err)
	}
	defer db.Close()
	err = db.Ping()
	if err != nil {
		log.Fatalf("Can't ping database: %v", err)
	}
	//pgquery := "SELECT DISTINCT(sigma), COUNT(*) FROM joup GROUP BY sigma ORDER BY count(*) DESC"
	pgquery := "SELECT COUNT(*) FROM joup"
	rowa := db.QueryRow(pgquery)
	err = rowa.Scan(&CCount)
	if err != nil {
		panic(err)
	}

	query := "SELECT DISTINCT(sigma), COUNT(*) FROM joup GROUP BY sigma ORDER BY count(*) DESC"
	rows, err := db.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	products := []product{}

	for rows.Next() {
		p := product{}
		err := rows.Scan(&p.count, &p.sigma)
		if err != nil {
			fmt.Println(err)
			continue
		}
		products = append(products, p)
	}

	fmt.Print("<table>")
	for _, p := range products {
		fmt.Print("<tr><td>")
		fmt.Print(p.count, "</td><td></td><td>", p.sigma)
		fmt.Println("</td></tr>")

	}
	fmt.Println("</table>")

	fmt.Println("Count of records = ", CCount)
}

//SELECT DISTINCT(sigma), COUNT(*) FROM joup GROUP BY sigma
/*
SELECT DISTINCT(sigma), COUNT(*) FROM joup GROUP BY sigma HAVING COUNT(*)>2000 ORDER BY count(*) DESC
вывод сумм без повторений, кол-во больше 2000
row := db.QueryRow("select * from productdb.Products where id = ?", 2)
prod := product{}
err = row.Scan(&prod.id, &prod.model, &prod.company, &prod.price)
if err != nil{
    panic(err)
}
fmt.Println(prod.id, prod.model, prod.company, prod.price)
*/
